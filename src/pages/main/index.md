---
path: /
intro: Här äter vi & delar på god mat i form av lite mindre rätter. Mestiza står
  för blandning. Mariuxi Ingber Robles har komponerat mellanrätter från alla
  världens hörn i en enda röra som hon tagit med sig från barndom och resor
  världen över och gjort till sin egen version.  Hon använder från mer enkla
  ingredienser till lite mer luxiösa. Vi dricker grymma viner av alla dess slag,
  som Kiki Ingber valt ut med omsorg. Kaos på bordet är valet vi gör. Lämna
  mobilerna i fickorna. Äta, dricka, prata, äta dricka, prata...
menus:
  - dishes:
      - alternatives: []
        name: "Ash Tanur (Israeliskt sotat bröd) med frön           "
        price: "45"
      - alternatives: []
        name: Hel kronärtskocka/ citron & vitlöksdressing/ blåbär & lavendelmajo
        price: "150"
      - alternatives: []
        name: Örtmarinerad Buffalo Mozzarella & Zucchiniblomma
        price: "160"
      - alternatives: []
        name: safransagnolotti / Kastanj/ brynt kryddigt smör
        price: "165"
      - alternatives: []
        name: Bakad Karameliserad Butternut Squash/ Rabarberredution/ rostade
          pistagenötter
        price: "140"
      - alternatives: []
        name: Tysk Vit Sparris/ Filippinska Pilinötter/ apelsinsmör
        price: "185"
      - alternatives: []
        name: Pulpo/ Favaröra/ cubansk Mojo
        price: "170"
      - alternatives: []
        name: Svenska Havskräftor, Cajun style (begränsat antal)
        price: "175"
      - alternatives: []
        name: Kalvcarpaccio/ Purjodressing/ Aji Limo/ Kapris/ Savoykål/ Agrietti
        price: "165"
      - alternatives: []
        name: Porchetta/ Örter/ Sky/ Murklor/ Vattenmelonrettika
        price: "175"
  - dishes:
      - alternatives:
          - name: Hel trallrik
            price: "360"
        name: Jamon Iberico de Bellota, 42 månader, Halv tallrik
        price: "190"
      - alternatives: []
        name: Jamon Iberico de Bellota, 42 månader, Hel tallrik
        price: "360"
      - alternatives: []
        name: Ostar liten tallrik
        price: "150"
      - alternatives: []
        name: Ostar stor tallrik
        price: "250"
  - dishes:
      - alternatives: []
        name: Sardeller, sardiner, Pulpo, Blåmusslor eller hjärtmusslor mm. i konserver,
          serveras med vitlöksbröd och citron. Priser finner ni på bardisken.
  - dishes:
      - alternatives: []
        name: Bär i Zabaione, toppad med mjuk maräng & fänkålssocker
        price: "120"
      - alternatives: []
        name: "Baskisk Cheesecake "
        price: "120"
menusCatering: []
info: |-
  Ons-Tors 17:00-23:00
  Fre-Lör 16:00-23:00 


  031 55 06 62
  info@mestizagbg.se
  Skanstorget 10, 411 22 Göteborg
---
